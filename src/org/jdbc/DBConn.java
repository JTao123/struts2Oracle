package org.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConn {
	
	private Connection conn=null;
	public DBConn(){
		this.conn=this.getConnection();//获取数据库连接
	}
	private Connection getConnection() {
		Connection conn=null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XSGY","scott","tiger");
		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
	public Connection getconn(){
		return conn;
	}
}
