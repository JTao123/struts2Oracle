package org.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.vo.Student;

public class StudentJdbc {
	private Connection conn = null;
	private PreparedStatement psmt = null;
	private ResultSet rs = null;

	public StudentJdbc() {

	}

	public Connection getConn() {
		try {
			if (this.conn == null || this.conn.isClosed()) {
				DBConn mc = new DBConn();
				this.conn = mc.getconn();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	// 添加学生
	public Student addStudent(Student student) {
		String sql1 = "insert into XSB(xh,xm,xb,cssj,zy,zxf,bz) values(?,?,?,?,?,?,?)";
		String sql2 = "insert into XSZP(xh,zp) values(?,?)";
		try {
			psmt = this.getConn().prepareStatement(sql1);
			psmt.setString(1, student.getXh());
			psmt.setString(2, student.getXm());
			psmt.setString(3, student.getXb());
			psmt.setTimestamp(4, new Timestamp(student.getCssj().getTime()));
			psmt.setString(5, student.getZy());
			psmt.setInt(6, student.getZxf());
			psmt.setString(7, student.getBz());
			psmt.execute();

			// *添加学生照片
			psmt = this.getConn().prepareStatement(sql2);
			psmt.setString(1, student.getXh());
			psmt.setBytes(2, student.getZp());
			psmt.execute();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				psmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return student;
	}

	// 查询所有学生
	public List<Student> showStudent() {
		String sql = "select * from XSB";
		List studentList = new ArrayList();
		try {
			psmt = this.getConn().prepareStatement(sql);
			rs = psmt.executeQuery();
			while (rs.next()) {
				Student student = new Student();
				student.setXh(rs.getString("xh"));
				student.setXm(rs.getString("xm"));
				student.setXb(rs.getString("xb"));
				student.setCssj(rs.getDate("cssj"));
				student.setZy(rs.getString("zy"));
				student.setZxf(rs.getInt("zxf"));
				student.setBz(rs.getString("bz"));

				studentList.add(student); // 将student对象放入到ArrayList中
			}
			return studentList; // 返回给控制器
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
					rs = null;
				}
				if (psmt != null) {
					psmt.close();
					psmt = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return studentList;
	}

	// 查询一个学生
	public Student showOneStudent(String xh) {
		ResultSet rs = null;

		String sql1 = "select * from XSB where xh=" + xh;
		String sql2 = "select zp from XSZP where xh=" + xh;
		Student student = new Student();

		try {
			psmt = this.getConn().prepareStatement(sql1);
			rs = psmt.executeQuery();
			/** 查询一个学生 */
			while (rs.next()) {
				student.setXh(rs.getString("xh"));
				student.setXm(rs.getString("xm"));
				student.setXb(rs.getString("xb"));
				student.setCssj(rs.getDate("cssj"));
				student.setZy(rs.getString("zy"));
				student.setZxf(rs.getInt("zxf"));
				student.setBz(rs.getString("bz"));
			}

			psmt = this.getConn().prepareStatement(sql2);
			rs = psmt.executeQuery();
			while (rs.next()) {
				student.setZp(rs.getBytes("zp"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				psmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return student;
	}

	// 删除一个学生
	public void deleteStudent(String xh) {
		String sql = "delete from XSB where xh=" + xh;
		try {
			psmt = this.getConn().prepareStatement(sql);
			psmt.execute(); // 将该学生的信息从数据库中删除
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				psmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	// 更新一个学生
	public Student updateSaveStudent(Student student) {
		String sql1 = "update XSB set xh=?,xm=?,xb=?,cssj=?,zy=?,zxf=?,bz=? where xh="+ student.getXh();

		String sql2 = "update XSZP set xh=?,zp=? where xh="+ student.getXh();



		try {
			psmt = this.getConn().prepareStatement(sql1);
			psmt.setString(1, student.getXh());
			psmt.setString(2, student.getXm());
			psmt.setString(3, student.getXb());
			System.out.println(student.getCssj());
			psmt.setTimestamp(4, new Timestamp( student.getCssj().getTime()));
			psmt.setString(5, student.getZy());
			psmt.setInt(6, student.getZxf());
			psmt.setString(7, student.getBz());
			psmt.execute();							

			psmt = this.getConn().prepareStatement(sql2);
			psmt.setString(1, student.getXh());
			psmt.setBytes(2, student.getZp());
			psmt.execute();							
			}
			
	    catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				psmt.close();						
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				conn.close();						
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return student;	
	}

}
