package org.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.vo.Course;
import org.vo.Score;
import org.vo.Student;

public class ScoreJdbc {
	
	private Connection conn=null;
	private ResultSet rs=null;
	private PreparedStatement psmt=null;
	public ScoreJdbc(){
		
	}
	public Connection getConn(){
		try{
			if(this.conn==null||this.conn.isClosed()){
				DBConn mc=new DBConn();
				this.conn=mc.getconn();
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return conn;
	}
	public List<Course> showCourse()throws SQLException{
		String sql="select * from KCB";
		List<Course>courseList=new ArrayList<Course>();
		try{
			psmt=this.getConn().prepareStatement(sql);
			rs=psmt.executeQuery();
			while(rs.next()){
				Course course=new Course();
				course.setKch(rs.getString("kch"));
				course.setKcm(rs.getString("kcm"));
				courseList.add(course);
			}
			return courseList;
				
			}catch(Exception e){
				e.printStackTrace();
		}finally{
			try{
				psmt.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
			try{
				conn.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
			
		}
		return courseList;
	}
	//查询所有学生
	public List<Student>showStudent()throws SQLException{
		String sql="select * from XSB";
		List<Student>studentList=new ArrayList<Student>();
		try{
			psmt=this.getConn().prepareStatement(sql);
			rs=psmt.executeQuery();
			while(rs.next()){
				Student student = new Student();
				student.setXh(rs.getString("xh"));
				student.setXm(rs.getString("xm"));
				studentList.add(student);
				
			}
			return studentList;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				psmt.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
			try{
				conn.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		return studentList;
	}
	//添加成绩
	public Score addScore(Score score){
		CallableStatement stmt=null;
		try{
			conn=this.getConn();
			stmt=conn.prepareCall("{call CJ_Data(?,?,?)}");
			stmt.setString(1, score.getXh());
			stmt.setString(2, score.getKch());
			stmt.setInt(3, score.getCj());
			stmt.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				stmt.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
			try{
				conn.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		return score;
	}

}
