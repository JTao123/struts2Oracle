package org.vo;

import java.io.Serializable;
import java.util.Date;

public class Student implements Serializable{
	
	private String xh;
	private String xm;
	private String xb;
	private Date cssj;
	private String zy;
	private int zxf;
	private String bz;//note
	private byte[] zp;//photo,�ֽ�����
	
	public Student(){
		
	}

	public Student(String xm, String xb, Date cssj, String zy, int zxf, String bz, byte[] zp) {
		super();
		this.xm = xm;
		this.xb = xb;
		this.cssj = cssj;
		this.zy = zy;
		this.zxf = zxf;
		this.bz = bz;
		this.zp = zp;
	}

	public String getXh() {
		return xh;
	}

	public void setXh(String xh) {
		this.xh = xh;
	}

	public String getXm() {
		return xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

	public String getXb() {
		return xb;
	}

	public void setXb(String xb) {
		this.xb = xb;
	}

	public Date getCssj() {
		return cssj;
	}

	public void setCssj(Date cssj) {
		this.cssj = cssj;
	}

	public String getZy() {
		return zy;
	}

	public void setZy(String zy) {
		this.zy = zy;
	}

	public int getZxf() {
		return zxf;
	}

	public void setZxf(int zxf) {
		this.zxf = zxf;
	}

	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}

	public byte[] getZp() {
		return zp;
	}

	public void setZp(byte[] zp) {
		this.zp = zp;
	}
	
}
