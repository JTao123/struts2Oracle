package org.vo;

import java.io.Serializable;

public class Course implements Serializable{
	private String kch;
	private String kcm;
	private int kkxq;
	private int xs;
	private int xf;
	public Course(){
		
	}
	public Course(String kch, String kcm, int kkxq, int xs, int xf) {
		super();
		this.kch = kch;
		this.kcm = kcm;
		this.kkxq = kkxq;
		this.xs = xs;
		this.xf = xf;
	}
	public String getKch() {
		return kch;
	}
	public void setKch(String kch) {
		this.kch = kch;
	}
	public String getKcm() {
		return kcm;
	}
	public void setKcm(String kcm) {
		this.kcm = kcm;
	}
	public int getKkxq() {
		return kkxq;
	}
	public void setKkxq(int kkxq) {
		this.kkxq = kkxq;
	}
	public int getXs() {
		return xs;
	}
	public void setXs(int xs) {
		this.xs = xs;
	}
	public int getXf() {
		return xf;
	}
	public void setXf(int xf) {
		this.xf = xf;
	}
	

}
