package org.vo;

public class Score {
	private String xh;
	private String kch;
	private int cj;
	public Score(){
		
	}
	public Score(String xh, String kch, int cj) {
		super();
		this.xh = xh;
		this.kch = kch;
		this.cj = cj;
	}
	public String getXh() {
		return xh;
	}
	public void setXh(String xh) {
		this.xh = xh;
	}
	public String getKch() {
		return kch;
	}
	public void setKch(String kch) {
		this.kch = kch;
	}
	public int getCj() {
		return cj;
	}
	public void setCj(int cj) {
		this.cj = cj;
	}

}
