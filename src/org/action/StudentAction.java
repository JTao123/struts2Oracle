package org.action;

import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.jdbc.StudentJdbc;
import org.vo.Student;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class StudentAction extends ActionSupport {
	
	private static final long serialVersionUID = 1L;
	private Student student;
	private File zpfile;
	private List<Student> studentList;
	// ALT+SHIFT+S
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	public File getZpfile() {
		return zpfile;
	}
	public void setZpfile(File zpfile) {
		this.zpfile = zpfile;
	}
	
	public String execute() throws Exception{
		StudentJdbc studentJ=new StudentJdbc();
		//收集表单数据
		if(this.getZpfile()!=null){
			//创建文件输入流，用于读取图片内容
			FileInputStream fis=new FileInputStream(this.getZpfile());
			//创建字节类型数组，用于存放图片的二进制数据
			byte[] buffer=new byte[fis.available()];
			fis.read(buffer);
			student.setZp(buffer);
		}
			studentJ.addStudent(student);
		return SUCCESS;
	}
	//查询所有学生
	public String showAllStudent(){
		Student student1 = new Student();
		StudentJdbc studentJ = new StudentJdbc();
		try {
			studentList = studentJ.showStudent();			
		} catch (Exception e) {
			e.printStackTrace();
		}
		Map request = (Map) ActionContext.getContext().get("request");  
		request.put("studentList", studentList);			
		return SUCCESS;
	}
	//查询一个学生信息
	public String showOneStudent() throws Exception{
		Student student1 = new Student();
		StudentJdbc studentJ = new StudentJdbc();
		student1 = studentJ.showOneStudent(student.getXh());  
		Map request = (Map) ActionContext.getContext().get("request");
		request.put("student1", student1);					
		return SUCCESS;
	}
	//删除一个学生信息
	public String  deleteStudent() throws Exception {
		Student student1 = new Student();
		StudentJdbc studentJ = new StudentJdbc();
		studentJ.deleteStudent(student.getXh());
		return SUCCESS;
	}
	//更新一个学生信息
	public String  updateSaveStudent() throws Exception {
		StudentJdbc studentJ = new StudentJdbc();
		//收集表单数据
		if (this.getZpfile() != null) {
			// 创建文件输入流用于读取照片信息
			FileInputStream fis = new FileInputStream(this.getZpfile());		
			byte[] buffer = new byte[fis.available()];	
			fis.read(buffer);
			student.setZp(buffer);
		}
		studentJ.updateSaveStudent(student);				
		return SUCCESS;
	}
}
