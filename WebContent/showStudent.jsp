<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
<body bgcolor="D9DFAA" background="image/bgcolor1.jpg" style="background-repeat:no-repeat;background-position:center">
<center>
<h2>学生信息一览</h2><hr>
	<table border="1">
	<tr align="center">
		<td>学号</td><td>姓名</td><td>性别</td><td>专业</td>
		<td>出生时间</td><td>总学分</td><td>备注</td><td>详细信息</td>
		<td colspan ="2">操作</td>
	</tr>
	<s:iterator value="#request.studentList" id="xs">
	<tr>
		<td><s:property value="#xs.xh"/></td>
		<td><s:property value="#xs.xm"/></td>
	    <td><s:property value="#xs.xb"/></td>
		<td><s:property value="#xs.zy"/></td>
		<td><s:date name="#xs.cssj" format="yyyy-MM-dd"/> </td>
		<td><s:property value="#xs.zxf"/></td>
		<td><s:property value="#xs.bz"/></td>
		<td><a href="showOneStudent.action?student.xh=<s:property value="#xs.xh"/>">详细信息
			</a></td>
		<td><a href="deleteStudent.action?student.xh=<s:property value="#xs.xh"/>" onClick=
			"if(!confirm('确定删除该信息吗？'))return false;else return true;">删除</a> </td>
		<td><a href="updateStudent.action?student.xh=<s:property value="#xs.xh"/>">修改</a> </td>
	</tr>
	</s:iterator>
</table>
<p>
</p>
</center>
</body>
</html>