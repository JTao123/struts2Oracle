<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
<body bgcolor="D9DFAA" background="image/bgcolor1.jpg" style="background-repeat:no-repeat;background-position:center">
	<center>
	<h3>录入学生成绩</h3><hr>
	<s:form action="addScore" method="post">
		<table border="1" cellspacing="1" cellpadding="8" width="400">
		<tr>
			<td width="100">请选择学生：</td>
			<td>
			<select name="score.xh">
			<s:iterator id="xs" value="#request.studentList">
				<option value="<s:property value="#xs.xh"/>"><s:property value="#xs.xm" />
				</option>
			</s:iterator>
			</select>
			</td>
		</tr>
		<tr>
			<td width="100">请选择课程：</td>
			<td>
			<select name="score.kch">
				<s:iterator id="kc" value="#request.courseList">
					<option value="<s:property value="#kc.kch"/>">
						<s:property value="#kc.kcm" />
					</option>
				</s:iterator>
			</select>
			</td>
		</tr>
		<tr>
			<s:textfield label="成绩" name="score.cj" value="" size="14"></s:textfield>
		</tr>
		</table>
		<input type="submit" value="录入" />
		<input type="reset" value="重置" />
	</s:form>
	</center>
</body>
</html>
