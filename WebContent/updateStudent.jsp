<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
<body bgcolor="D9DFAA" background="image/bgcolor1.jpg" style="background-repeat:no-repeat;background-position:center">

<center>
	<h3>该学生信息</h3><hr>
	<s:set name="xs" value="#request.student1"></s:set>
	<s:form action="updateSaveStudent.action" method="post" enctype="multipart/form-data" >
	<table border="1" width="400">
		<tr>
			<td>学号：</td><td><input type="text" name="student.xh" value="<s:property 
				value="#xs.xh"/>" readonly/></td>
		</tr>
		<tr>
			<td>姓名：</td><td><input type="text" name="student.xm" value="<s:property 
				value="#xs.xm"/>"/></td>
		</tr>
		<tr>
		<s:radio list="{'男','女'}" value="#xs.xb" label="性别" 	name="student.xb"></s:radio>
		</tr>
		<tr>
			<td>专业：</td><td><input type="text" name="student.zy" value="<s:property 
				value="#xs.zy"/>"/></td>
		</tr>
		<tr>
			<td>出生时间：</td>
			<td><input type="text" name="student.cssj" value="<s:date name="#xs.cssj" 			
				format="yyyy-MM-dd"/>"/></td>
		</tr>
		<tr>
			<td>总学分：</td><td><input type="text" name="student.zxf" value="<s:property 		
				value="#xs.zxf"/>"/></td>
		</tr>
		<tr>
			<td>备注：</td><td><input type="text" name="student.bz" value="<s:property 		
				value="#xs.bz"/>"/></td>
		</tr>
		<tr>
			<td>照片</td><td><input type="file" name="zpfile" value=""/></td>
		</tr>

	</table>
	<p>
	<input type="submit" value="修改"/>
	<input type="button" value="返回" onclick="javascript:history.back();"/>
	</s:form>
	</center>
</body>
</html>
