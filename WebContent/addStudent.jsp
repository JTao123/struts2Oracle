<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
<body bgcolor="D9DFAA" background="image/bgcolor1.jpg" style="background-repeat:no-repeat;background-position:center">
<center>
	<h3>请填写新生资料</h3><hr>
		<s:form action="addStudent.action" method="post" enctype="multipart/form-data" >
			<table border="1">
			<tr><s:textfield name="student.xh" label="学号"></s:textfield></tr>
			<tr><s:textfield name="student.xm" label="姓名"></s:textfield></tr>
			<tr><s:radio name="student.xb" value="男" list="{'男','女'}" label="性别" /></tr>
			<tr><s:textfield name="student.zy" label="专业"></s:textfield></tr>
			<tr><s:textfield name="student.cssj" label="出生时间"></s:textfield></tr>
			<tr><s:textfield name="student.zxf" label="总学分"></s:textfield></tr>
			<tr><s:textarea name="student.bz" label="备注"></s:textarea></tr>
			<tr><s:file name="zpfile" label="照片"></s:file></tr>
			</table> 
			<p>
			<input type="submit" value="添加"/>
			<input type="reset" value="重置"/>
		</s:form>
</center>
</body>
</html>