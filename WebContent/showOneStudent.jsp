<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
<body bgcolor="D9DFAA" background="image/bgcolor1.jpg" style="background-repeat:no-repeat;background-position:center">
<center>
<h3>该学生信息</h3><hr>		
<s:set name="xs" value="#request.student1"></s:set>
<table border="1">
  <tr>
     <td>
		  <table border="0">
			<tr>
				<td>学号：</td>
				<td width="200"><s:property value="#xs.xh"/></td>
			</tr>
			<tr>
				<td>姓名：</td>
				<td width="100"><s:property value="#xs.xm"/></td>
			</tr>
			<tr>
				<td>性别：</td>
				<td width="100"><s:property value="#xs.xb"/></td>
			</tr>
			<tr>
				<td>专业：</td>
				<td width="100"><s:property value="#xs.zy"/></td>
			</tr>
			<tr>
				<td>出生时间：</td>
				<td width="100"><s:date name="#xs.cssj" format="yyyy-MM-dd"/></td>
			</tr>
			<tr>
				<td>总学分：</td>
				<td width="100"><s:property value="#xs.zxf"/></td>
			</tr>
            <tr>
		        <td>备注：</td>
		        <td width="100"><s:property value="#xs.bz"/></td>
            </tr>
		 </table>
    </td>
    <td>
         <table>
            <tr>
                <td align="center">照片</td>
            </tr>
            <tr>
                <td width="100"> <img src="getImage.action?student.xh=<s:property value="#xs.xh"/>" width="150"> 
                </td>
            </tr>
         </table>   
    </td>
   </tr>
</table>
       <input type="button" value="返回" onClick="javaScript:history.back()" />
</center>
</body>
</html>